/*
// Projeto SO - exercicio 2, version 1
// Sistemas Operativos, DEI/IST/ULisboa 2016-17
*/

#include "commandlinereader.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <strings.h>
#include <time.h>

#define COMANDO_DEBITAR "debitar"
#define COMANDO_CREDITAR "creditar"
#define COMANDO_TRANSFERIR "transferir"
#define COMANDO_LER_SALDO "lerSaldo"
#define COMANDO_SIMULAR "simular"
#define COMANDO_SAIR "sair"
#define COMANDO_ARG_SAIR_AGORA "agora"
#define COMANDO_SAIR_TERMINAL "sair-terminal"

#define OP_LER_SALDO 0
#define OP_CREDITAR  1
#define OP_DEBITAR   2
#define OP_SAIR      3
#define OP_TRANSFERIR   4
#define OP_SIMULAR  5


#define MAXARGS 4
#define BUFFER_SIZE 100


typedef struct
{
  int operacao;
  int idConta;
  int idContaDestino;
  int valor;
  int pid;
} comando_t;

void redirectPipe(int sig){
  printf("Erro ao enviar comando. (Pipe inexistente)\n");
  exit(EXIT_FAILURE);
}

int main (int argc, char** argv){
  char *args[MAXARGS + 1];
  char buffer[BUFFER_SIZE];
  comando_t cmd;
  int send_cmd_through_pipe;
  int waitOutput;
  int pipeInput, pipeOutput;
  int pid = getpid();
  char uniquePipeID[30];
  sprintf(uniquePipeID, "i-banco-terminal-%d", pid);
  unlink(uniquePipeID);

  if((pipeInput = open(argv[1], O_WRONLY|O_CREAT, S_IWRITE)) < 0){ //Responsavel por enviar comandos
    perror("Impossivel abrir pipe de input");
    exit(EXIT_FAILURE);
  }

  if(mkfifo(uniquePipeID, 0777) < 0){
    perror("Impossivel criar pipe de output.");
    exit(EXIT_FAILURE);
  }

  if(signal(SIGPIPE, redirectPipe) == SIG_ERR) {
    perror("Erro ao definir signal.");
    exit(EXIT_FAILURE);
  }

  while (1) {
    int numargs;

    numargs = readLineArguments(args, MAXARGS+1, buffer, BUFFER_SIZE);

    send_cmd_through_pipe = 0; /* default is NO (do not send) */
    waitOutput = 1;
    if (numargs > 0 && strcmp(args[0], COMANDO_SAIR_TERMINAL)==0){
      close(pipeInput);
      unlink(uniquePipeID);
      exit(EXIT_SUCCESS);
    }
    /* EOF (end of file) do stdin ou comando "sair" */
    if (numargs < 0 ||
        (numargs > 0 && (strcmp(args[0], COMANDO_SAIR) == 0))) {
          cmd.operacao = OP_SAIR;
          if (numargs > 1 && (strcmp(args[1], COMANDO_ARG_SAIR_AGORA) == 0))
            cmd.idConta = -1; //Flag para identificar que o comando leva a informacao de que e para sair agora
          send_cmd_through_pipe = 1;
          waitOutput = 0;
        }
    else if (numargs == 0){
      /* Nenhum argumento; ignora e volta a pedir */
      continue;
    }

    /* Debitar */
    else if (strcmp(args[0], COMANDO_DEBITAR) == 0) {
      if (numargs < 3) {
        printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_DEBITAR);
        continue;
      }
      cmd.operacao = OP_DEBITAR;
      cmd.idConta = atoi(args[1]);
      cmd.valor = atoi(args[2]);
      cmd.pid = pid;
      send_cmd_through_pipe = 1;
    }

    /* Creditar */
    else if (strcmp(args[0], COMANDO_CREDITAR) == 0) {
      if (numargs < 3) {
        printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_CREDITAR);
        continue;
      }
      cmd.operacao = OP_CREDITAR;
      cmd.idConta = atoi(args[1]);
      cmd.valor = atoi(args[2]);
      cmd.pid = pid;

      send_cmd_through_pipe = 1;
    }

    else if (strcmp(args[0], COMANDO_TRANSFERIR) == 0) {
     if (numargs < 4) {
        printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_TRANSFERIR);
        continue;
      }
      cmd.operacao = OP_TRANSFERIR;
      cmd.idConta = atoi(args[1]);
      cmd.idContaDestino = atoi(args[2]);
      cmd.valor = atoi(args[3]);
      cmd.pid = pid;

      send_cmd_through_pipe = 1;
    }

    /* Ler Saldo */
    else if (strcmp(args[0], COMANDO_LER_SALDO) == 0) {
      if (numargs < 2) {
        printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_LER_SALDO);
        continue;
      }
      cmd.operacao = OP_LER_SALDO;
      cmd.idConta = atoi(args[1]);
      cmd.pid = pid;

      send_cmd_through_pipe = 1;
    }

    /* Simular */
    else if (strcmp(args[0], COMANDO_SIMULAR) == 0) {
      if (numargs < 2) {
        printf("%s: Sintaxe inválida, tente de novo.\n", COMANDO_SIMULAR);
        continue;
      }
      cmd.operacao = OP_SIMULAR;
      cmd.valor = atoi(args[1]);

      send_cmd_through_pipe = 1;
      waitOutput = 0;
    }

    else
      printf("Comando desconhecido. Tente de novo.\n");


    if(send_cmd_through_pipe) {
      time_t start_t, end_t;
      write(pipeInput, &cmd, sizeof(comando_t));
      if(waitOutput){
          time(&start_t);
          char mensagem[100];
          bzero(mensagem, 100);
          if((pipeOutput = open(uniquePipeID, O_RDONLY, S_IRWXU)) < 0){ //Responsavel por receber outputs do i-banco
            perror("Impossivel abrir pipe de output");
            exit(EXIT_FAILURE);
          }
          read(pipeOutput, mensagem , 100);
          time(&end_t);
          printf("%s",mensagem);
          printf("Tempo de execucao: %f s.\n", difftime(end_t, start_t));
          close(pipeOutput);
      }
    }
  }
}
